//
//  ViewController.swift
//  quadra
//
//  Created by COTEMIG on 31/03/22.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    @IBOutlet weak var table:UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.dataSource = self
    }


}

